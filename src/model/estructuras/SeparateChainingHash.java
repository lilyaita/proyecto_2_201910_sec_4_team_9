package model.estructuras;

import model.estructuras.Queue;
/**
 * 
 * @author USER
 * Tomado de  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 * @param <Key>
 * @param <Value>
 */
public class SeparateChainingHash <Key extends Comparable<Key>, Value> implements IHashGenerica<Key, Value>
{
	/**
	 * Capacidad inicial 
	 */
	private static final int INIT_CAPACITY = 97;
	/**
	 * Numero de pares (Key, Value) en la tabla
	 */
	private int n;
	/**
	 * Tama�o inicial del arreglo
	 */
	private int m;                               
	/**
	 * Arreglo de linked-list symbol tables
	 */
	private SequentialSearchST<Key, Value>[] st;  
	/**
	 * Contador de n�mero de rehashes
	 */
	private int nurehash;	 

	/**
	 * Initializes an empty symbol table.
	 */
	public SeparateChainingHash() {
		this(INIT_CAPACITY);
	} 

	/**
	 * Initializes an empty symbol table with {@code m} chains.
	 * @param m the initial number of chains
	 */
	public SeparateChainingHash(int m) {
		this.m = m;
		st = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[m];
		for (int i = 0; i < m; i++)
			st[i] = new SequentialSearchST<Key, Value>();
		nurehash = 0;
	} 
	/**
	 * Resize the hash table to have the given number of chains,rehashing all of the keys
	 * @param chains
	 */
	private void rehash(int chains) {
		SeparateChainingHash<Key, Value> temp = new SeparateChainingHash<Key, Value>(chains);
		nurehash++;
		for (int i = 0; i < m; i++) {
			for (Key key : st[i].keys()) {
				temp.put(key, st[i].get(key));
			}
		}
		this.m  = temp.m;
		this.n  = temp.n;
		this.st = temp.st;
	}
	/**
	 * hash value between 0 and m-1
	 * @param key
	 * @return valor
	 */
	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % m;
	} 

	/**
	 * Returns the number of key-value pairs in this symbol table.
	 *
	 * @return the number of key-value pairs in this symbol table
	 */
	public int size() {
		return n;
	} 

	/**
	 * Returns true if this symbol table is empty.
	 *
	 * @return {@code true} if this symbol table is empty;
	 *         {@code false} otherwise
	 */
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Returns true if this symbol table contains the specified key.
	 * @param  key the key
	 * @return {@code true} if this symbol table contains {@code key};
	 *         {@code false} otherwise
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public boolean contains(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to contains() is null");
		return get(key) != null;
	} 

	/**
	 * Returns the value associated with the specified key in this symbol table.
	 *
	 * @param  key the key
	 * @return the value associated with {@code key} in the symbol table;
	 *         {@code null} if no such value
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public Value get(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
		int i = hash(key);
		return st[i].get(key);
	} 

	/**
	 * Inserts the specified key-value pair into the symbol table, overwriting the old 
	 * value with the new value if the symbol table already contains the specified key.
	 * Deletes the specified key (and its associated value) from this symbol table
	 * if the specified value is {@code null}.
	 *
	 * @param  key the key
	 * @param  val the value
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void put(Key key, Value val) {
		if (key == null) throw new IllegalArgumentException("first argument to put() is null");
		if (val == null) {
			delete(key);
			return;
		}

		
		if (((double)n)/m > 5.0) rehash((2*m)+1);

		int i = hash(key);
		if (!st[i].contains(key)) n++;
		st[i].put(key, val);
	} 

	/**
	 * Removes the specified key and its associated value from this symbol table     
	 * (if the key is in this symbol table).    
	 *
	 * @param  key the key
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public Value delete(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");

		int i = hash(key);
		if (st[i].contains(key)) n--;
		Value res = st[i].delete(key);

		
		if(n > 0 && n <= m*5) rehash((m/2)+1);
		return res;
	} 
	/**
	 * Return keys in symbol table as an Iterable
	 * @return keys in symbol table as an Iterable
	 */

	public Iterable<Key> keys() {
		Queue<Key> queue = new Queue<Key>();
		for (int i = 0; i < m; i++) {
			for (Key key : st[i].keys())
				queue.enqueue(key);
		}
		return queue;
	} 
	/**
     * Returns number of rehashes 
     * @return rehashes
     */
    public int getrehashes ()
    {
    	return nurehash;
    }
    /**
	 * Returns the size of array
	 *
	 * @return the the size of array
	 */
	public int sizeArray() {
		return m;
	} 
}
