/**
 * Tomado de https://algs4.cs.princeton.edu
 */

package model.estructuras;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.NoSuchElementException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class BST<Key extends Comparable<Key>, Value> {

	
	public final static class StdIn {

	    /*** begin: section (1 of 2) of code duplicated from In to StdIn. */
	    
	    // assume Unicode UTF-8 encoding
	    private static final String CHARSET_NAME = "UTF-8";

	    // assume language = English, country = US for consistency with System.out.
	    private static final Locale LOCALE = Locale.US;

	    // the default token separator; we maintain the invariant that this value
	    // is held by the scanner's delimiter between calls
	    private static final Pattern WHITESPACE_PATTERN = Pattern.compile("\\p{javaWhitespace}+");

	    // makes whitespace significant
	    private static final Pattern EMPTY_PATTERN = Pattern.compile("");

	    // used to read the entire input
	    private static final Pattern EVERYTHING_PATTERN = Pattern.compile("\\A");

	    /*** end: section (1 of 2) of code duplicated from In to StdIn. */

	    private static Scanner scanner;
	 
	    // it doesn't make sense to instantiate this class
	    private StdIn() { }

	    //// begin: section (2 of 2) of code duplicated from In to StdIn,
	    //// with all methods changed from "public" to "public static"

	   /**
	     * Returns true if standard input is empty (except possibly for whitespace).
	     * Use this method to know whether the next call to {@link #readString()}, 
	     * {@link #readDouble()}, etc will succeed.
	     *
	     * @return {@code true} if standard input is empty (except possibly
	     *         for whitespace); {@code false} otherwise
	     */
	    public static boolean isEmpty() {
	        return !scanner.hasNext();
	    }

	   /**
	     * Returns true if standard input has a next line.
	     * Use this method to know whether the
	     * next call to {@link #readLine()} will succeed.
	     * This method is functionally equivalent to {@link #hasNextChar()}.
	     *
	     * @return {@code true} if standard input has more input (including whitespace);
	     *         {@code false} otherwise
	     */
	    public static boolean hasNextLine() {
	        return scanner.hasNextLine();
	    }

	    /**
	     * Returns true if standard input has more input (including whitespace).
	     * Use this method to know whether the next call to {@link #readChar()} will succeed.
	     * This method is functionally equivalent to {@link #hasNextLine()}.
	     *
	     * @return {@code true} if standard input has more input (including whitespace);
	     *         {@code false} otherwise
	     */
	    public static boolean hasNextChar() {
	        scanner.useDelimiter(EMPTY_PATTERN);
	        boolean result = scanner.hasNext();
	        scanner.useDelimiter(WHITESPACE_PATTERN);
	        return result;
	    }


	   /**
	     * Reads and returns the next line, excluding the line separator if present.
	     *
	     * @return the next line, excluding the line separator if present;
	     *         {@code null} if no such line
	     */
	    public static String readLine() {
	        String line;
	        try {
	            line = scanner.nextLine();
	        }
	        catch (NoSuchElementException e) {
	            line = null;
	        }
	        return line;
	    }

	    /**
	     * Reads and returns the next character.
	     *
	     * @return the next {@code char}
	     * @throws NoSuchElementException if standard input is empty
	     */
	    public static char readChar() {
	        try {
	            scanner.useDelimiter(EMPTY_PATTERN);
	            String ch = scanner.next();
	            assert ch.length() == 1 : "Internal (Std)In.readChar() error!"
	                + " Please contact the authors.";
	            scanner.useDelimiter(WHITESPACE_PATTERN);
	            return ch.charAt(0);
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attempts to read a 'char' value from standard input, "
	                                           + "but no more tokens are available");
	        }
	    }  


	   /**
	     * Reads and returns the remainder of the input, as a string.
	     *
	     * @return the remainder of the input, as a string
	     * @throws NoSuchElementException if standard input is empty
	     */
	    public static String readAll() {
	        if (!scanner.hasNextLine())
	            return "";

	        String result = scanner.useDelimiter(EVERYTHING_PATTERN).next();
	        // not that important to reset delimeter, since now scanner is empty
	        scanner.useDelimiter(WHITESPACE_PATTERN); // but let's do it anyway
	        return result;
	    }


	   /**
	     * Reads the next token  and returns the {@code String}.
	     *
	     * @return the next {@code String}
	     * @throws NoSuchElementException if standard input is empty
	     */
	    public static String readString() {
	        try {
	            return scanner.next();
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attempts to read a 'String' value from standard input, "
	                                           + "but no more tokens are available");
	        }
	    }

	   /**
	     * Reads the next token from standard input, parses it as an integer, and returns the integer.
	     *
	     * @return the next integer on standard input
	     * @throws NoSuchElementException if standard input is empty
	     * @throws InputMismatchException if the next token cannot be parsed as an {@code int}
	     */
	    public static int readInt() {
	        try {
	            return scanner.nextInt();
	        }
	        catch (InputMismatchException e) {
	            String token = scanner.next();
	            throw new InputMismatchException("attempts to read an 'int' value from standard input, "
	                                           + "but the next token is \"" + token + "\"");
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attemps to read an 'int' value from standard input, "
	                                           + "but no more tokens are available");
	        }

	    }

	   /**
	     * Reads the next token from standard input, parses it as a double, and returns the double.
	     *
	     * @return the next double on standard input
	     * @throws NoSuchElementException if standard input is empty
	     * @throws InputMismatchException if the next token cannot be parsed as a {@code double}
	     */
	    public static double readDouble() {
	        try {
	            return scanner.nextDouble();
	        }
	        catch (InputMismatchException e) {
	            String token = scanner.next();
	            throw new InputMismatchException("attempts to read a 'double' value from standard input, "
	                                           + "but the next token is \"" + token + "\"");
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attempts to read a 'double' value from standard input, "
	                                           + "but no more tokens are available");
	        }
	    }

	   /**
	     * Reads the next token from standard input, parses it as a float, and returns the float.
	     *
	     * @return the next float on standard input
	     * @throws NoSuchElementException if standard input is empty
	     * @throws InputMismatchException if the next token cannot be parsed as a {@code float}
	     */
	    public static float readFloat() {
	        try {
	            return scanner.nextFloat();
	        }
	        catch (InputMismatchException e) {
	            String token = scanner.next();
	            throw new InputMismatchException("attempts to read a 'float' value from standard input, "
	                                           + "but the next token is \"" + token + "\"");
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attempts to read a 'float' value from standard input, "
	                                           + "but there no more tokens are available");
	        }
	    }

	   /**
	     * Reads the next token from standard input, parses it as a long integer, and returns the long integer.
	     *
	     * @return the next long integer on standard input
	     * @throws NoSuchElementException if standard input is empty
	     * @throws InputMismatchException if the next token cannot be parsed as a {@code long}
	     */
	    public static long readLong() {
	        try {
	            return scanner.nextLong();
	        }
	        catch (InputMismatchException e) {
	            String token = scanner.next();
	            throw new InputMismatchException("attempts to read a 'long' value from standard input, "
	                                           + "but the next token is \"" + token + "\"");
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attempts to read a 'long' value from standard input, "
	                                           + "but no more tokens are available");
	        }
	    }

	   /**
	     * Reads the next token from standard input, parses it as a short integer, and returns the short integer.
	     *
	     * @return the next short integer on standard input
	     * @throws NoSuchElementException if standard input is empty
	     * @throws InputMismatchException if the next token cannot be parsed as a {@code short}
	     */
	    public static short readShort() {
	        try {
	            return scanner.nextShort();
	        }
	        catch (InputMismatchException e) {
	            String token = scanner.next();
	            throw new InputMismatchException("attempts to read a 'short' value from standard input, "
	                                           + "but the next token is \"" + token + "\"");
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attempts to read a 'short' value from standard input, "
	                                           + "but no more tokens are available");
	        }
	    }

	   /**
	     * Reads the next token from standard input, parses it as a byte, and returns the byte.
	     *
	     * @return the next byte on standard input
	     * @throws NoSuchElementException if standard input is empty
	     * @throws InputMismatchException if the next token cannot be parsed as a {@code byte}
	     */
	    public static byte readByte() {
	        try {
	            return scanner.nextByte();
	        }
	        catch (InputMismatchException e) {
	            String token = scanner.next();
	            throw new InputMismatchException("attempts to read a 'byte' value from standard input, "
	                                           + "but the next token is \"" + token + "\"");
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attempts to read a 'byte' value from standard input, "
	                                           + "but no more tokens are available");
	        }
	    }

	    /**
	     * Reads the next token from standard input, parses it as a boolean,
	     * and returns the boolean.
	     *
	     * @return the next boolean on standard input
	     * @throws NoSuchElementException if standard input is empty
	     * @throws InputMismatchException if the next token cannot be parsed as a {@code boolean}:
	     *    {@code true} or {@code 1} for true, and {@code false} or {@code 0} for false,
	     *    ignoring case
	     */
	    public static boolean readBoolean() {
	        try {
	            String token = readString();
	            if ("true".equalsIgnoreCase(token))  return true;
	            if ("false".equalsIgnoreCase(token)) return false;
	            if ("1".equals(token))               return true;
	            if ("0".equals(token))               return false;
	            throw new InputMismatchException("attempts to read a 'boolean' value from standard input, "
	                                           + "but the next token is \"" + token + "\"");
	        }
	        catch (NoSuchElementException e) {
	            throw new NoSuchElementException("attempts to read a 'boolean' value from standard input, "
	                                           + "but no more tokens are available");
	        }

	    }

	    /**
	     * Reads all remaining tokens from standard input and returns them as an array of strings.
	     *
	     * @return all remaining tokens on standard input, as an array of strings
	     */
	    public static String[] readAllStrings() {
	        // we could use readAll.trim().split(), but that's not consistent
	        // because trim() uses characters 0x00..0x20 as whitespace
	        String[] tokens = WHITESPACE_PATTERN.split(readAll());
	        if (tokens.length == 0 || tokens[0].length() > 0)
	            return tokens;

	        // don't include first token if it is leading whitespace
	        String[] decapitokens = new String[tokens.length-1];
	        for (int i = 0; i < tokens.length - 1; i++)
	            decapitokens[i] = tokens[i+1];
	        return decapitokens;
	    }

	    /**
	     * Reads all remaining lines from standard input and returns them as an array of strings.
	     * @return all remaining lines on standard input, as an array of strings
	     */
	    public static String[] readAllLines() {
	        ArrayList<String> lines = new ArrayList<String>();
	        while (hasNextLine()) {
	            lines.add(readLine());
	        }
	        return lines.toArray(new String[lines.size()]);
	    }

	    /**
	     * Reads all remaining tokens from standard input, parses them as integers, and returns
	     * them as an array of integers.
	     * @return all remaining integers on standard input, as an array
	     * @throws InputMismatchException if any token cannot be parsed as an {@code int}
	     */
	    public static int[] readAllInts() {
	        String[] fields = readAllStrings();
	        int[] vals = new int[fields.length];
	        for (int i = 0; i < fields.length; i++)
	            vals[i] = Integer.parseInt(fields[i]);
	        return vals;
	    }

	    /**
	     * Reads all remaining tokens from standard input, parses them as longs, and returns
	     * them as an array of longs.
	     * @return all remaining longs on standard input, as an array
	     * @throws InputMismatchException if any token cannot be parsed as a {@code long}
	     */
	    public static long[] readAllLongs() {
	        String[] fields = readAllStrings();
	        long[] vals = new long[fields.length];
	        for (int i = 0; i < fields.length; i++)
	            vals[i] = Long.parseLong(fields[i]);
	        return vals;
	    }

	    /**
	     * Reads all remaining tokens from standard input, parses them as doubles, and returns
	     * them as an array of doubles.
	     * @return all remaining doubles on standard input, as an array
	     * @throws InputMismatchException if any token cannot be parsed as a {@code double}
	     */
	    public static double[] readAllDoubles() {
	        String[] fields = readAllStrings();
	        double[] vals = new double[fields.length];
	        for (int i = 0; i < fields.length; i++)
	            vals[i] = Double.parseDouble(fields[i]);
	        return vals;
	    }
	    
	    //// end: section (2 of 2) of code duplicated from In to StdIn
	    
	    
	    // do this once when StdIn is initialized
	    static {
	        resync();
	    }

	    /**
	     * If StdIn changes, use this to reinitialize the scanner.
	     */
	    private static void resync() {
	        setScanner(new Scanner(new java.io.BufferedInputStream(System.in), CHARSET_NAME));
	    }
	    
	    private static void setScanner(Scanner scanner) {
	        StdIn.scanner = scanner;
	        StdIn.scanner.useLocale(LOCALE);
	    }

	   /**
	     * Reads all remaining tokens, parses them as integers, and returns
	     * them as an array of integers.
	     * @return all remaining integers, as an array
	     * @throws InputMismatchException if any token cannot be parsed as an {@code int}
	     * @deprecated Replaced by {@link #readAllInts()}.
	     */
	    @Deprecated
	    public static int[] readInts() {
	        return readAllInts();
	    }

	   /**
	     * Reads all remaining tokens, parses them as doubles, and returns
	     * them as an array of doubles.
	     * @return all remaining doubles, as an array
	     * @throws InputMismatchException if any token cannot be parsed as a {@code double}
	     * @deprecated Replaced by {@link #readAllDoubles()}.
	     */
	    @Deprecated
	    public static double[] readDoubles() {
	        return readAllDoubles();
	    }

	   /**
	     * Reads all remaining tokens and returns them as an array of strings.
	     * @return all remaining tokens, as an array of strings
	     * @deprecated Replaced by {@link #readAllStrings()}.
	     */
	    @Deprecated
	    public static String[] readStrings() {
	        return readAllStrings();
	    }


	    /**
	     * Interactive test of basic functionality.
	     *
	     * @param args the command-line arguments
	     */
	    public static void main(String[] args) {

	        StdOut.print("Type a string: ");
	        String s = StdIn.readString();
	        StdOut.println("Your string was: " + s);
	        StdOut.println();

	        StdOut.print("Type an int: ");
	        int a = StdIn.readInt();
	        StdOut.println("Your int was: " + a);
	        StdOut.println();

	        StdOut.print("Type a boolean: ");
	        boolean b = StdIn.readBoolean();
	        StdOut.println("Your boolean was: " + b);
	        StdOut.println();

	        StdOut.print("Type a double: ");
	        double c = StdIn.readDouble();
	        StdOut.println("Your double was: " + c);
	        StdOut.println();
	    }

	}
	
	public final static class StdOut {

	    // force Unicode UTF-8 encoding; otherwise it's system dependent
	    private static final String CHARSET_NAME = "UTF-8";

	    // assume language = English, country = US for consistency with StdIn
	    private static final Locale LOCALE = Locale.US;

	    // send output here
	    private static PrintWriter out;

	    // this is called before invoking any methods
	    static {
	        try {
	            out = new PrintWriter(new OutputStreamWriter(System.out, CHARSET_NAME), true);
	        }
	        catch (UnsupportedEncodingException e) {
	            System.out.println(e);
	        }
	    }

	    // don't instantiate
	    private StdOut() { }

	   /**
	     * Closes standard output.
	     * @deprecated Calling close() permanently disables standard output;
	     *             subsequent calls to StdOut.println() or System.out.println()
	     *             will no longer produce output on standard output.
	     */
	    @Deprecated
	    public static void close() {
	        out.close();
	    }

	   /**
	     * Terminates the current line by printing the line-separator string.
	     */
	    public static void println() {
	        out.println();
	    }

	   /**
	     * Prints an object to this output stream and then terminates the line.
	     *
	     * @param x the object to print
	     */
	    public static void println(Object x) {
	        out.println(x);
	    }

	   /**
	     * Prints a boolean to standard output and then terminates the line.
	     *
	     * @param x the boolean to print
	     */
	    public static void println(boolean x) {
	        out.println(x);
	    }

	   /**
	     * Prints a character to standard output and then terminates the line.
	     *
	     * @param x the character to print
	     */
	    public static void println(char x) {
	        out.println(x);
	    }

	   /**
	     * Prints a double to standard output and then terminates the line.
	     *
	     * @param x the double to print
	     */
	    public static void println(double x) {
	        out.println(x);
	    }

	   /**
	     * Prints an integer to standard output and then terminates the line.
	     *
	     * @param x the integer to print
	     */
	    public static void println(float x) {
	        out.println(x);
	    }

	   /**
	     * Prints an integer to standard output and then terminates the line.
	     *
	     * @param x the integer to print
	     */
	    public static void println(int x) {
	        out.println(x);
	    }

	   /**
	     * Prints a long to standard output and then terminates the line.
	     *
	     * @param x the long to print
	     */
	    public static void println(long x) {
	        out.println(x);
	    }

	   /**
	     * Prints a short integer to standard output and then terminates the line.
	     *
	     * @param x the short to print
	     */
	    public static void println(short x) {
	        out.println(x);
	    }

	   /**
	     * Prints a byte to standard output and then terminates the line.
	     * <p>
	     * To write binary data, see {@link BinaryStdOut}.
	     *
	     * @param x the byte to print
	     */
	    public static void println(byte x) {
	        out.println(x);
	    }

	   /**
	     * Flushes standard output.
	     */
	    public static void print() {
	        out.flush();
	    }

	   /**
	     * Prints an object to standard output and flushes standard output.
	     * 
	     * @param x the object to print
	     */
	    public static void print(Object x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints a boolean to standard output and flushes standard output.
	     * 
	     * @param x the boolean to print
	     */
	    public static void print(boolean x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints a character to standard output and flushes standard output.
	     * 
	     * @param x the character to print
	     */
	    public static void print(char x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints a double to standard output and flushes standard output.
	     * 
	     * @param x the double to print
	     */
	    public static void print(double x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints a float to standard output and flushes standard output.
	     * 
	     * @param x the float to print
	     */
	    public static void print(float x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints an integer to standard output and flushes standard output.
	     * 
	     * @param x the integer to print
	     */
	    public static void print(int x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints a long integer to standard output and flushes standard output.
	     * 
	     * @param x the long integer to print
	     */
	    public static void print(long x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints a short integer to standard output and flushes standard output.
	     * 
	     * @param x the short integer to print
	     */
	    public static void print(short x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints a byte to standard output and flushes standard output.
	     *
	     * @param x the byte to print
	     */
	    public static void print(byte x) {
	        out.print(x);
	        out.flush();
	    }

	   /**
	     * Prints a formatted string to standard output, using the specified format
	     * string and arguments, and then flushes standard output.
	     *
	     *
	     * @param format the <a href = "http://docs.oracle.com/javase/7/docs/api/java/util/Formatter.html#syntax">format string</a>
	     * @param args   the arguments accompanying the format string
	     */
	    public static void printf(String format, Object... args) {
	        out.printf(LOCALE, format, args);
	        out.flush();
	    }

	   /**
	     * Prints a formatted string to standard output, using the locale and
	     * the specified format string and arguments; then flushes standard output.
	     *
	     * @param locale the locale
	     * @param format the <a href = "http://docs.oracle.com/javase/7/docs/api/java/util/Formatter.html#syntax">format string</a>
	     * @param args   the arguments accompanying the format string
	     */
	    public static void printf(Locale locale, String format, Object... args) {
	        out.printf(locale, format, args);
	        out.flush();
	    }

	   /**
	     * Unit tests some of the methods in {@code StdOut}.
	     *
	     * @param args the command-line arguments
	     */
	    public static void main(String[] args) {

	        // write to stdout
	        StdOut.println("Test");
	        StdOut.println(17);
	        StdOut.println(true);
	        StdOut.printf("%.6f\n", 1.0/7.0);
	    }

	}

	
	
	
	
	
	
	
	
	
	
	
	
    private Node root;             // root of BST

    private class Node {
        private Key key;           // sorted by key
        private Value val;         // associated data
        private Node left, right;  // left and right subtrees
        private int size;          // number of nodes in subtree

        public Node(Key key, Value val, int size) {
            this.key = key;
            this.val = val;
            this.size = size;
        }
    }

    /**
     * Initializes an empty symbol table.
     */
    public BST() {
    }

    /**
     * Returns true if this symbol table is empty.
     * @return {@code true} if this symbol table is empty; {@code false} otherwise
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns the number of key-value pairs in this symbol table.
     * @return the number of key-value pairs in this symbol table
     */
    public int size() {
        return size(root);
    }

    // return number of key-value pairs in BST rooted at x
    private int size(Node x) {
        if (x == null) return 0;
        else return x.size;
    }

    /**
     * Does this symbol table contain the given key?
     *
     * @param  key the key
     * @return {@code true} if this symbol table contains {@code key} and
     *         {@code false} otherwise
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public boolean contains(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key) != null;
    }

    /**
     * Returns the value associated with the given key.
     *
     * @param  key the key
     * @return the value associated with the given key if the key is in the symbol table
     *         and {@code null} if the key is not in the symbol table
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Value get(Key key) {
        return get(root, key);
    }

    private Value get(Node x, Key key) {
        if (key == null) throw new IllegalArgumentException("calls get() with a null key");
        if (x == null) return null;
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) return get(x.left, key);
        else if (cmp > 0) return get(x.right, key);
        else              return x.val;
    }

    /**
     * Inserts the specified key-value pair into the symbol table, overwriting the old 
     * value with the new value if the symbol table already contains the specified key.
     * Deletes the specified key (and its associated value) from this symbol table
     * if the specified value is {@code null}.
     *
     * @param  key the key
     * @param  val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(Key key, Value val) {
        if (key == null) throw new IllegalArgumentException("calls put() with a null key");
        if (val == null) {
            delete(key);
            return;
        }
        root = put(root, key, val);
        assert check();
    }

    private Node put(Node x, Key key, Value val) {
        if (x == null) return new Node(key, val, 1);
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) x.left  = put(x.left,  key, val);
        else if (cmp > 0) x.right = put(x.right, key, val);
        else              x.val   = val;
        x.size = 1 + size(x.left) + size(x.right);
        return x;
    }


    /**
     * Removes the smallest key and associated value from the symbol table.
     *
     * @throws NoSuchElementException if the symbol table is empty
     */
    public void deleteMin() {
        if (isEmpty()) throw new NoSuchElementException("Symbol table underflow");
        root = deleteMin(root);
        assert check();
    }

    private Node deleteMin(Node x) {
        if (x.left == null) return x.right;
        x.left = deleteMin(x.left);
        x.size = size(x.left) + size(x.right) + 1;
        return x;
    }

    /**
     * Removes the largest key and associated value from the symbol table.
     *
     * @throws NoSuchElementException if the symbol table is empty
     */
    public void deleteMax() {
        if (isEmpty()) throw new NoSuchElementException("Symbol table underflow");
        root = deleteMax(root);
        assert check();
    }

    private Node deleteMax(Node x) {
        if (x.right == null) return x.left;
        x.right = deleteMax(x.right);
        x.size = size(x.left) + size(x.right) + 1;
        return x;
    }

    /**
     * Removes the specified key and its associated value from this symbol table     
     * (if the key is in this symbol table).    
     *
     * @param  key the key
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void delete(Key key) {
        if (key == null) throw new IllegalArgumentException("calls delete() with a null key");
        root = delete(root, key);
        assert check();
    }

    private Node delete(Node x, Key key) {
        if (x == null) return null;

        int cmp = key.compareTo(x.key);
        if      (cmp < 0) x.left  = delete(x.left,  key);
        else if (cmp > 0) x.right = delete(x.right, key);
        else { 
            if (x.right == null) return x.left;
            if (x.left  == null) return x.right;
            Node t = x;
            x = min(t.right);
            x.right = deleteMin(t.right);
            x.left = t.left;
        } 
        x.size = size(x.left) + size(x.right) + 1;
        return x;
    } 


    /**
     * Returns the smallest key in the symbol table.
     *
     * @return the smallest key in the symbol table
     * @throws NoSuchElementException if the symbol table is empty
     */
    public Key min() {
        if (isEmpty()) throw new NoSuchElementException("calls min() with empty symbol table");
        return min(root).key;
    } 

    private Node min(Node x) { 
        if (x.left == null) return x; 
        else                return min(x.left); 
    } 

    /**
     * Returns the largest key in the symbol table.
     *
     * @return the largest key in the symbol table
     * @throws NoSuchElementException if the symbol table is empty
     */
    public Key max() {
        if (isEmpty()) throw new NoSuchElementException("calls max() with empty symbol table");
        return max(root).key;
    } 

    private Node max(Node x) {
        if (x.right == null) return x; 
        else                 return max(x.right); 
    } 

    /**
     * Returns the largest key in the symbol table less than or equal to {@code key}.
     *
     * @param  key the key
     * @return the largest key in the symbol table less than or equal to {@code key}
     * @throws NoSuchElementException if there is no such key
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Key floor(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to floor() is null");
        if (isEmpty()) throw new NoSuchElementException("calls floor() with empty symbol table");
        Node x = floor(root, key);
        if (x == null) return null;
        else return x.key;
    } 

    private Node floor(Node x, Key key) {
        if (x == null) return null;
        int cmp = key.compareTo(x.key);
        if (cmp == 0) return x;
        if (cmp <  0) return floor(x.left, key);
        Node t = floor(x.right, key); 
        if (t != null) return t;
        else return x; 
    } 

    public Key floor2(Key key) {
        return floor2(root, key, null);
    }

    private Key floor2(Node x, Key key, Key best) {
        if (x == null) return best;
        int cmp = key.compareTo(x.key);
        if      (cmp  < 0) return floor2(x.left, key, best);
        else if (cmp  > 0) return floor2(x.right, key, x.key);
        else               return x.key;
    } 

    /**
     * Returns the smallest key in the symbol table greater than or equal to {@code key}.
     *
     * @param  key the key
     * @return the smallest key in the symbol table greater than or equal to {@code key}
     * @throws NoSuchElementException if there is no such key
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Key ceiling(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to ceiling() is null");
        if (isEmpty()) throw new NoSuchElementException("calls ceiling() with empty symbol table");
        Node x = ceiling(root, key);
        if (x == null) return null;
        else return x.key;
    }

    private Node ceiling(Node x, Key key) {
        if (x == null) return null;
        int cmp = key.compareTo(x.key);
        if (cmp == 0) return x;
        if (cmp < 0) { 
            Node t = ceiling(x.left, key); 
            if (t != null) return t;
            else return x; 
        } 
        return ceiling(x.right, key); 
    } 

    /**
     * Return the key in the symbol table whose rank is {@code k}.
     * This is the (k+1)st smallest key in the symbol table.
     *
     * @param  k the order statistic
     * @return the key in the symbol table of rank {@code k}
     * @throws IllegalArgumentException unless {@code k} is between 0 and
     *        <em>n</em>�1
     */
    public Key select(int k) {
        if (k < 0 || k >= size()) {
            throw new IllegalArgumentException("argument to select() is invalid: " + k);
        }
        Node x = select(root, k);
        return x.key;
    }

    // Return key of rank k. 
    private Node select(Node x, int k) {
        if (x == null) return null; 
        int t = size(x.left); 
        if      (t > k) return select(x.left,  k); 
        else if (t < k) return select(x.right, k-t-1); 
        else            return x; 
    } 

    /**
     * Return the number of keys in the symbol table strictly less than {@code key}.
     *
     * @param  key the key
     * @return the number of keys in the symbol table strictly less than {@code key}
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public int rank(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to rank() is null");
        return rank(key, root);
    } 

    // Number of keys in the subtree less than key.
    private int rank(Key key, Node x) {
        if (x == null) return 0; 
        int cmp = key.compareTo(x.key); 
        if      (cmp < 0) return rank(key, x.left); 
        else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
        else              return size(x.left); 
    } 

    /**
     * Returns all keys in the symbol table as an {@code Iterable}.
     * To iterate over all of the keys in the symbol table named {@code st},
     * use the foreach notation: {@code for (Key key : st.keys())}.
     *
     * @return all keys in the symbol table
     */
    public Iterable<Key> keys() {
        if (isEmpty()) return new Queue<Key>();
        return keys(min(), max());
    }

    /**
     * Returns all keys in the symbol table in the given range,
     * as an {@code Iterable}.
     *
     * @param  lo minimum endpoint
     * @param  hi maximum endpoint
     * @return all keys in the symbol table between {@code lo} 
     *         (inclusive) and {@code hi} (inclusive)
     * @throws IllegalArgumentException if either {@code lo} or {@code hi}
     *         is {@code null}
     */
    public Iterable<Key> keys(Key lo, Key hi) {
        if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
        if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

        Queue<Key> queue = new Queue<Key>();
        keys(root, queue, lo, hi);
        return queue;
    } 

    private void keys(Node x, Queue<Key> queue, Key lo, Key hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) keys(x.left, queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
        if (cmphi > 0) keys(x.right, queue, lo, hi); 
    } 

    /**
     * Returns the number of keys in the symbol table in the given range.
     *
     * @param  lo minimum endpoint
     * @param  hi maximum endpoint
     * @return the number of keys in the symbol table between {@code lo} 
     *         (inclusive) and {@code hi} (inclusive)
     * @throws IllegalArgumentException if either {@code lo} or {@code hi}
     *         is {@code null}
     */
    public int size(Key lo, Key hi) {
        if (lo == null) throw new IllegalArgumentException("first argument to size() is null");
        if (hi == null) throw new IllegalArgumentException("second argument to size() is null");

        if (lo.compareTo(hi) > 0) return 0;
        if (contains(hi)) return rank(hi) - rank(lo) + 1;
        else              return rank(hi) - rank(lo);
    }

    /**
     * Returns the height of the BST (for debugging).
     *
     * @return the height of the BST (a 1-node tree has height 0)
     */
    public int height() {
        return height(root);
    }
    private int height(Node x) {
        if (x == null) return -1;
        return 1 + Math.max(height(x.left), height(x.right));
    }

    /**
     * Returns the keys in the BST in level order (for debugging).
     *
     * @return the keys in the BST in level order traversal
     */
    public Iterable<Key> levelOrder() {
        Queue<Key> keys = new Queue<Key>();
        Queue<Node> queue = new Queue<Node>();
        queue.enqueue(root);
        while (!queue.isEmpty()) {
            Node x = queue.dequeue();
            if (x == null) continue;
            keys.enqueue(x.key);
            queue.enqueue(x.left);
            queue.enqueue(x.right);
        }
        return keys;
    }

  /*************************************************************************
    *  Check integrity of BST data structure.
    ***************************************************************************/
    private boolean check() {
        if (!isBST())            StdOut.println("Not in symmetric order");
        if (!isSizeConsistent()) StdOut.println("Subtree counts not consistent");
        if (!isRankConsistent()) StdOut.println("Ranks not consistent");
        return isBST() && isSizeConsistent() && isRankConsistent();
    }

    // does this binary tree satisfy symmetric order?
    // Note: this test also ensures that data structure is a binary tree since order is strict
    private boolean isBST() {
        return isBST(root, null, null);
    }

    // is the tree rooted at x a BST with all keys strictly between min and max
    // (if min or max is null, treat as empty constraint)
    // Credit: Bob Dondero's elegant solution
    private boolean isBST(Node x, Key min, Key max) {
        if (x == null) return true;
        if (min != null && x.key.compareTo(min) <= 0) return false;
        if (max != null && x.key.compareTo(max) >= 0) return false;
        return isBST(x.left, min, x.key) && isBST(x.right, x.key, max);
    } 

    // are the size fields correct?
    private boolean isSizeConsistent() { return isSizeConsistent(root); }
    private boolean isSizeConsistent(Node x) {
        if (x == null) return true;
        if (x.size != size(x.left) + size(x.right) + 1) return false;
        return isSizeConsistent(x.left) && isSizeConsistent(x.right);
    } 

    // check that ranks are consistent
    private boolean isRankConsistent() {
        for (int i = 0; i < size(); i++)
            if (i != rank(select(i))) return false;
        for (Key key : keys())
            if (key.compareTo(select(rank(key))) != 0) return false;
        return true;
    }


    /**
     * Unit tests the {@code BST} data type.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) { 
        BST<String, Integer> st = new BST<String, Integer>();
        for (int i = 0; !StdIn.isEmpty(); i++) {
            String key = StdIn.readString();
            if ((st.size() > 1) && (st.floor(key) != st.floor2(key)))
                throw new RuntimeException("floor() function inconsistent");
            st.put(key, i);
        }

        for (String s : st.levelOrder())
            StdOut.println(s + " " + st.get(s));

        StdOut.println();

        for (String s : st.keys())
            StdOut.println(s + " " + st.get(s));
    }
}