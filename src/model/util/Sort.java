package model.util;
/**
 * Sell : Algorithms, 4th Edition.  Robert Sedgewick and Kevin Wayne
 * Merge: Sicua presentaciones.
 * @author USER
 *
 */
public class Sort {

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo ShellSort
		int n = datos.length;

		int h = 1;
		while (h < n/3) h = 3*h + 1; 

		while (h >= 1) {
			// h-sort the array
			for (int i = h; i < n; i++) {
				for (int j = i; j >= h && less(datos[j], datos[j-h]); j -= h) {
					exchange(datos, j, j-h);
				}
			}
			assert isHsorted(datos, h); 
			h /= 3;
		}
		assert isSorted(datos);
	}
	/**
	 * Ordenar por merge
	 * @param datos conjunto de datos
	 * @param aux Arreglo auxiliar
	 * @param lo  Posici�n inicial
	 * @param mid mitad
	 * @param hi Posici�n final
	 */
	private static void merge(Comparable[] datos, Comparable[] aux, int lo, int mid, int hi){
		for (int k = lo; k <= hi; k++)
			aux[k] = datos[k];
		int i=lo, j = mid+1;
		for (int k = lo; k <= hi; k++) {
			if (i > mid) datos[k] = aux[j++];
			else if (j > hi ) datos[k] = aux[i++];
			else if (less(aux[j], aux[i])) datos[k] = aux[j++];
			else datos[k] = aux[i++];
		}


	}

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo MergeSort
		int N = datos.length;
		Comparable[] aux = new Comparable[N];
		for (int sz = 1; sz < N; sz = sz+sz)
			for (int lo = 0; lo < N-sz; lo += sz+sz)
				merge(datos, aux, lo, lo+sz-1, Math.min(lo+sz+sz-1, N-1));

	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos ) {

		Quick.sort(datos);
	}

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		// TODO implementar
		return (v.compareTo(w) < 0);
	}
	/**
	 * Verificar si esta ordenado todo el arreglo
	 * @param datos contenedor de datos
	 * @return True si esta rodenado, false si no
	 */
	private static boolean isSorted(Comparable[] datos) {
		for (int i = 1; i < datos.length; i++)
			if (less(datos[i], datos[i-1])) return false;
		return true;
	}
	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// TODO implementar
		Comparable k = datos[i];
		datos[i] = datos[j];
		datos[j] = k;
	}
	/**
	 * El arreglo H esta ordenado
	 * @param datos
	 * @param h
	 * @return True si esta rodenado, false si no
	 */
	private static boolean isHsorted(Comparable[] datos, int h) {
		for (int i = h; i < datos.length; i++)
			if (less(datos[i], datos[i-h])) return false;
		return true;
	}

}
