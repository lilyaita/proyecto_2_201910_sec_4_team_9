package view;

import model.estructuras.Heap;
import model.estructuras.PriorityQueue;
import model.vo.LocationVo;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() {
		
	}
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");
		System.out.println("1. Cargar datos del semstre");
		
		System.out.println("8. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printDatosMuestra( int nMuestra, Comparable [ ] muestra)
	{
		for ( Comparable dato : muestra)
		{	System.out.println(  dato.toString() );    }
	}
	public void printLocation( int n, PriorityQueue<LocationVo> pq, Heap<LocationVo> hp)
	{
		n= Math.min(n,hp.size());
		n--;
		while (n>=0  )
		{
			System.out.println(hp.delMax());
			n--;
		}
	}
	
	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}
	
}