package controller;

import java.io.FileReader;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import model.estructuras.IQueue;
import model.estructuras.IStack;
import model.estructuras.Queue;
import model.estructuras.Stack;
import model.util.Quick;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller 
{
	private static final String meses []= {
			"./data/January_2018.csv",
			"./data/February_2018.csv",
			"./data/March_2018.csv",
			"./data/April_2018.csv",
			"./data/May_2018.csv",
			"./data/June_2018.csv",
			"./data/July_2018.csv",
			"./data/August_2018.csv",
			"./data/September_2018.csv",
			"./data/October_2018.csv",
			"./data/November_2018.csv",
			"./data/December_2018.csv"


	};

	/**
	 * View
	 */
	private MovingViolationsManagerView view;
	/**
	 * Stack con los datos
	 */
	protected Stack<VOMovingViolations> datos; 
	/**
	 * Sabe si ya se leyeron los datos
	 */
	private boolean leidos; 
	/**
	 * El n�mero de cuatrimestre
	 */
	private int semestre; 
	/**
	 * el tama�o de datos del mesese
	 */
	private int tamanoMeses []; 

	private int totalDatos;

	private double xMax, yMax, xMin, yMin; 


	/**
	 * Constructor inicializa el total de datos en cero, crea la view y la lista
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();
		datos = new Stack<VOMovingViolations>(); 
		tamanoMeses = new int [6];
		totalDatos = 0; 
		xMax = yMax = xMin = yMin = 0.0; 
	}
	/**
	 * Corre el menu y seg�n lo que responda el usuario ejecuta
	 */
	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;


		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				view.printMessage("Ingrese el cuatrimestre que desea cargar: (1 o 2)");
				semestre = sc.nextInt(); 
				if(leidos == false)
				{
					try
					{
						long starTime = System.currentTimeMillis();
						loadMovingViolations();
						view.printMessage("Se demoro: "+ (System.currentTimeMillis()-starTime) + " ms" );
						leidos = true; 
						for (int i=0;i<6;i++)
							view.printMessage("Datos Mes "+(i+1)+": " + tamanoMeses[i]);
						view.printMessage("----------------------------");
						view.printMessage("Total datos: " + datos.size());
						view.printMessage("----------------------------");
						view.printMessage("Coordenadas (Max): " + "(" +xMax+","+yMax+")");
						view.printMessage("Coordenadas (Min): " + "(" +xMin+","+yMin+")");

					}
					catch(Exception e)
					{
						view.printMessage(e.getMessage());
					}
				}
				else
				{
					view.printMessage("Ya fue leido un semestre!");
				}
				break;
			case 8:	
				fin=true;
				semestre = 0;
				sc.close();
				break;
			}
		}
	}
	/**
	 * Carga el cuatrimestre seleccionado
	 * @return int el total de datos
	 * @throws Exception si no se logro cargar bien los archivos 
	 */
	public int loadMovingViolations() throws Exception
	{
		for (int i=0;i<6;i++)
		{

			FileReader fileReaderMes = new FileReader(meses[i+((semestre-1)*6)]);
			CSVReader csvReaderMes = new CSVReaderBuilder(fileReaderMes).withSkipLines(1).build(); 
			String[] row;
			int aumenta = (i+((semestre-1)*6))>=9 ? 1:0;
			while((row = csvReaderMes.readNext())!= null )
			{

				int objectId = Integer.parseInt(row[0]); 
				int totalPaid = (int)Double.parseDouble(row[9]);
				short fi = Short.parseShort(row[8]);
				short penalty1 = Short.parseShort(row[10]);
				double xCoord = Double.parseDouble(row[5]);
				double yCoord = Double.parseDouble(row[6]); 
				xMin = xCoord; 
				yMin = yCoord; 

				datos.push(new VOMovingViolations(objectId, totalPaid,  fi,  row[2], row[13+aumenta],

						row[12+aumenta],row[14+aumenta], row[15+aumenta], row[4], row[3], penalty1, xCoord, yCoord));
				tamanoMeses[i]++;

				if(xCoord > xMax)
				{
					xMax = xCoord; 
				}

				if(yCoord > yMax)
				{
					yMax = yCoord; 
				}

				if(xCoord < xMin)
				{
					xMin = xCoord; 
				}
				
				if(yCoord < yMin)
				{
					yMin = yCoord; 
				}
			}

		}
		return totalDatos;
	}


	
	
	public IStack<VOMovingViolations> obtenerRankingPorFranja(String date)
	{
		return null; 
	}
	
	public void ordenarInfraccionesPorCoordenas()
	{
		
	}
	
	public IStack<VOMovingViolations> buscarInfraccionesEnRango(String fechaInicial, String fechaFinal)
	{
		return null; 
	}
	
	public IStack<VOMovingViolations> obtenerRankingPorViolationCode(int code)
	{
		return null; 
	}
	
	public double calcularValorAcumulado()
	{
		return 0; 
	}
	
	public VOMovingViolations obtenerInformacionDeLocalizacion(int id)
	{
		return null;
	}
	
	public IStack<VOMovingViolations> obtenerInfraccionesEnRango(String inicial, String fin)
	{
		return null;
	}
	
	public IQueue<VOMovingViolations> rankingMayorInfracciones(double xCoord, double yCoord)
	{
		return null;
	}
	
	
	/**
	 * Verificar que OBJECTID es �nico en todos los datos
	 * @return Stack de OBJECTID repetidos
	 */
	public IStack<VOMovingViolations> verifyObjectIDIsUnique() 
	{
		IStack<VOMovingViolations> repetidos = new Stack<VOMovingViolations>(); 
		Comparable<VOMovingViolations> [] data = new Comparable[datos.size()];
		Iterator<VOMovingViolations> it = datos.iterator();
		int i = 0; 

		while(it.hasNext())
		{
			VOMovingViolations actual = it.next();
			actual.changeComparator(2);
			data[i] = actual; 
			i++; 
		}

		Quick.sort(data);
		i = 0; 
		while(i < data.length-1)
		{
			VOMovingViolations actual = (VOMovingViolations) data[i]; 
			VOMovingViolations siguiente = (VOMovingViolations) data[i+1];

			if(actual.objectId() == siguiente.objectId())
			{
				repetidos.push(actual);
			}

			i++;

		}

		return repetidos;
	}
	/**
	 * Verifica si los OBJECTID son unicos 
	 * @return true si los OBJECTID son unicos, false en caso contrario
	 */
	public boolean verifyObjectIDIsUniqueBoolean()
	{
		return verifyObjectIDIsUnique().isEmpty();
	}

	/**
	 * Dado	 un	 tipo	 de	 infracci�n	 (VIOLATIONCODE) informa el FINEAMT promedio cuando hubo accidente y cuando no lo hubo
	 * @param violationCode3 El tipo de infracci�n por la cual se busca
	 * @return arreglo de doubles con el promedio de accidente y noAccidente
	 */
	public double[] avgFineAmountByViolationCode(String violationCode3) {
		double accidente = 0.0;
		int contador = 0;
		double noAccidente = 0.0;

		Comparable<VOMovingViolations>[] ordenar = new Comparable[datos.size()];
		contador = ordenar.length;
		Iterator<VOMovingViolations> it = datos.iterator();
		int i = 0;

		while(it.hasNext())
		{
			VOMovingViolations actual = it.next();
			actual.changeComparator(5);
			ordenar[i] = actual;
			i++; 
		}

		Quick.sort(ordenar);
		i = 0;

		while(i < ordenar.length)
		{
			VOMovingViolations actual = (VOMovingViolations) ordenar[i]; 

			if(actual.getViolationcode().equals(violationCode3))
			{
				if(actual.getAccidentIndicator().equals("No"))
				{
					noAccidente += actual.getFineamt();
				}
				else if(actual.getAccidentIndicator().equals("Yes"))
				{
					accidente += actual.getFineamt();
				}
			}

			i++;
		}

		accidente = accidente/contador;
		noAccidente = noAccidente/contador;

		return new double [] {accidente , noAccidente};
	}


	/**
	 * Consultar	 infracciones	 donde	 la	 cantidad	 pagada	 (TOTALPAID) esta	 en	 un	 rango	
	 * dado. Se	 ordena por	 fecha	 de	 infracci�n. 
	 * @param limiteInf6 limite inferior de la cantidad pagada
	 * @param limiteSup6 limite superior de la cantidad pagada
	 * @param ascendente6 true si se quiere ordenado ascendentemente
	 * @return Stack con los VOMovingViolations
	 */
	public IStack<VOMovingViolations> getMovingViolationsByTotalPaid(double limiteInf6, double limiteSup6,	boolean ascendente6) {
		Stack<VOMovingViolations> res = new Stack<VOMovingViolations> ();
		int i = 0; 
		Comparable<VOMovingViolations> [] arr = new Comparable[datos.size()];
		Iterator<VOMovingViolations> it = datos.iterator();
		while(it.hasNext())
		{
			VOMovingViolations actual = it.next();
			actual.changeComparator(3 * (ascendente6?1:-1));
			arr[i] = actual; 
			i++; 
		}
		Quick.sort(arr);
		i = 0; 
		while(i < arr.length)
		{
			VOMovingViolations actual = (VOMovingViolations) arr[i]; 
			if (limiteInf6 < actual.getTotalPaid() && limiteSup6 > actual.getTotalPaid())
			{
				res.push(actual);
			}
			i++;
		}
		return res;
	}

	/**
	 * Dado	un	tipo	de	infracci�n	(VIOLATIONCODE)	informar	el	(FINEAMT)	promedio	y	su	
	 * desviaci�n	est�ndar
	 * @param violationCode8 violationCode dado
	 * @return Arreglo de double con el promedio y la desviaci�n est�ndar
	 */
	public double[] avgAndStdDevFineAmtOfMovingViolation(String violationCode8) {
		int i = 0;
		double res[] = new double [2];
		Comparable<VOMovingViolations> [] arr = new Comparable[datos.size()];
		Iterator<VOMovingViolations> it = datos.iterator();
		while(it.hasNext())
		{
			VOMovingViolations actual = it.next();
			actual.changeComparator(4);
			arr[i] = actual; 
			i++; 
		}
		i = 0; 
		long suma =0;
		int total=0;
		while(i < arr.length)
		{
			VOMovingViolations actual = (VOMovingViolations) arr[i]; 
			if (actual.getViolationcode().equals(violationCode8) )
			{
				suma+=actual.getFineamt();
				total++;
			}
			i++;
		}
		if (total!=0)
			res[0]=suma/(double)total;
		i = 0; 
		suma=0;
		while(i < arr.length)
		{
			VOMovingViolations actual = (VOMovingViolations) arr[i]; 
			if (actual.getViolationcode().equals(violationCode8) )
			{
				suma+=(actual.getFineamt()-res[0])*(actual.getFineamt()-res[0]);
			}
			i++;
		}
		if (total>0)
			res[1]=Math.sqrt((double)suma/(total-1));
		return res;
	}



	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		//		"dd/MM/yyyy'T'HH:mm:ss"
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("dd/MM/yyyy'T'HH:mm:ss"));
	}
}
