
/**
 * Tomado de N9 (APO 2 Honores) 
 * Creador de los JUnit tests:  Christian Camilo Aparicio Baquen 
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;


import model.estructuras.ListaDobementeEncadenada;
import model.util.Quick;


public class ListaDoblementeEncadenadaTest 
{

	private ListaDobementeEncadenada <String> lista;
	@Before
	public void setUpEscenario1()
	{
		lista = new ListaDobementeEncadenada <String>();
	}
	public void setUpEscenario2() 
	{
		lista = new ListaDobementeEncadenada <String>();
		lista.addLast("a");
	}
	public void setUpEscenario3() 
	{
		lista = new ListaDobementeEncadenada <String>();
		lista.addLast("a");
		lista.addLast("b");
		lista.addLast("c");
	}
	@Test
	public void constructor() 
	{
		assertEquals( 0,lista.size());
		assertTrue (lista.isEmpty());
		setUpEscenario2();
		assertEquals( 1, lista.size());
		assertTrue (!lista.isEmpty());
		
		
	}
	
	@Test
	public void removeFirstAndAddLast() 
	{
		try
		{
			lista.addLast("a");
			assertEquals(1,lista.size());
				
			lista.addLast("e");
			assertEquals( 2,lista.size());
			
			lista.addLast("b");
			assertEquals( 3,lista.size());
			

		}catch( Exception e)
		{
			fail("Deberia poder agregar"+e.getMessage());
		}
		try
		{
			
			
			assertEquals("a",lista.removeFirst());
			assertEquals( 2,lista.size());
			
			assertEquals( "e", lista.removeFirst());
			assertEquals( 1, lista.size());
			
			assertEquals("b",lista.removeFirst());
			assertEquals( 0,lista.size());
			
		}catch( Exception e)
		{
			fail("Deberia poder eliminar"+e.getMessage());
		}
		try 
		{
			lista.removeFirst();
			fail("No se puede eliminar");
		}catch( Exception e)
		{
			//Respuesta correcta
		}
		
	}
	@Test
	public void removeEndAndAddFirst() 
	{
		try
		{
			lista.addFirst("a");
			assertEquals(1,lista.size());
				
			lista.addFirst("e");
			assertEquals( 2,lista.size());
			
			lista.addFirst("b");
			assertEquals( 3,lista.size());
			
			assertEquals("b",lista.removeFirst());
			assertEquals( 2,lista.size());
			
			assertEquals( "e", lista.removeFirst());
			assertEquals( 1, lista.size());
			
			assertEquals("a",lista.removeFirst());
			assertEquals( 0,lista.size());
			
		}catch( Exception e)
		{
			fail("Deberia poder agregar");
		}
		try 
		{
			lista.removeFirst();
			fail("No se puede eliminar");
		}catch( Exception e)
		{
			//Respuesta correcta
		}
		
	}
	
	@Test
	public void sort ()
	{
		Character m []={'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m'};
		Character o[]= new Character[m.length];
		ListaDobementeEncadenada<Character> listaDoble = new ListaDobementeEncadenada<Character>();
		for (int i=0;i<m.length;i++)
		{
			listaDoble.addFirst(m[i]);
			o[i]= m[i];
		}
		
		Arrays.sort(m, new Comparator<Character>(){ 
			  
            @Override
            public int compare(Character c1, Character c2) 
            { 
                // ignoring case 
                return Character.compare(Character.toLowerCase(c1), 
                                        Character.toLowerCase(c2)); 
            } 
        });
		Quick.sort(o);
		for (int i=0;i<m.length;i++)
		{
			
			assertEquals(m[i], o[i]);
		}
	}
	
	
}
