


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.estructuras.SeparateChainingHash;
import model.estructuras.Stack;

public class SeparateChainingHashTest
{
	public class TestObject
	{
		private int key; 
		private String val;

		public TestObject(int key, String val)
		{
			this.key = key;
			this.val = val; 
		}

		public int getKey()
		{
			return key; 
		}

		public String getVal()
		{
			return val;
		}
	}

	private SeparateChainingHash<Integer, Stack<TestObject>> sep;
	private ArrayList<TestObject> arreglo; 


	@Before
	public void setUpEscenario()
	{
		sep = new SeparateChainingHash<>();
		arreglo = new ArrayList<>();
		arreglo.add(new TestObject(1, "C"));
		arreglo.add(new TestObject(2, "B"));
		arreglo.add(new TestObject(1, "A"));
		arreglo.add(new TestObject(4, "D")); 
		arreglo.add(new TestObject(2, "A")); 
		arreglo.add(new TestObject(6, "B")); 
	}
	
	@Test
	public void put()
	{
		for(TestObject a : arreglo)
		{
			try
			{
				Stack<TestObject> aux = sep.get(a.getKey()); 

				if(aux == null)
				{
					aux = new Stack<>(); 
				}
				aux.push(a);

				sep.put(a.getKey(), aux);
			}
			catch(Exception e)
			{
				fail("No deber�a lanzar excepcion");
			}
		}
	}
	
	@Test
	public void delete()
	{
		TestObject primero = arreglo.get(0);
		TestObject segundo = arreglo.get(2);
		
		Stack<TestObject> comparar = new Stack<>();
		comparar.push(primero);
		comparar.push(segundo);
		
		put();
		
		Stack<TestObject> val = sep.get(primero.key); 
		sep.delete(primero.key); 
		
		assertEquals(comparar.get(), val.get());
	}
	
	@Test
	public void get()
	{
		put(); 
		
		Stack<TestObject> fin = sep.get(6); 
		
		assertEquals(fin.get().key, arreglo.get(arreglo.size() -1).key);
		
	}

}
