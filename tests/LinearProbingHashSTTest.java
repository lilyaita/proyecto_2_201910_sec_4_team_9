

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import model.estructuras.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


import model.estructuras.Stack;

public class LinearProbingHashSTTest 
{
	
	public class TestObject
	{
		private int key; 
		private String val;
		
		public TestObject(int key, String val)
		{
			this.key = key;
			this.val = val; 
		}
		
		public int getKey()
		{
			return key; 
		}
		
		public String getVal()
		{
			return val;
		}
	}
	
	private LinearProbingHashST<Integer, Stack<TestObject>> linear;
	private ArrayList<TestObject> arreglo; 
	
	
	@Before
	public void setUpEscenario()
	{
		linear = new LinearProbingHashST<>();
		arreglo = new ArrayList<>();
		arreglo.add(new TestObject(1, "C"));
		arreglo.add(new TestObject(2, "B"));
		arreglo.add(new TestObject(1, "A"));
		arreglo.add(new TestObject(4, "D")); 
		arreglo.add(new TestObject(2, "A")); 
		arreglo.add(new TestObject(6, "B")); 
	}
	
	@Test
	public void put()
	{
		for(TestObject a : arreglo)
		{
			try
			{
				Stack<TestObject> aux = linear.get(a.getKey()); 
				
				if(aux == null)
				{
					aux = new Stack<>(); 
				}
				aux.push(a);
				
				linear.put(a.getKey(), aux);
			}
			catch(Exception e)
			{
				fail("No deber�a lanzar excepcion");
			}
		}
	}
	
	
	@Test
	public void delete()
	{	
		TestObject primero = arreglo.get(0);
		TestObject segundo = arreglo.get(2);
		
		Stack<TestObject> comparar = new Stack<>();
		comparar.push(primero);
		comparar.push(segundo);
		
		put();
		
		Stack<TestObject> val = linear.get(primero.key); 
		linear.delete(primero.key); 
		
		assertEquals(comparar.get(), val.get());	
	}
	
	@Test
	public void get()
	{
		put(); 
		
		Stack<TestObject> fin = linear.get(6); 
		
		assertEquals(fin.get().key, arreglo.get(arreglo.size() -1).key);
		
		
	}
}
