




import org.junit.Before;
import org.junit.Test;

import model.estructuras.*;


public class PruebasTabla {

	private SeparateChainingHash <Integer, Integer> separateHash;
	private LinearProbingHashST <Integer, Integer> linearProbingHash;
	@Before
	public void setUpEscenario()
	{
		separateHash = new SeparateChainingHash <Integer, Integer> (7);
		linearProbingHash = new LinearProbingHashST <Integer, Integer> (7);
		for (int i=0;i<10000;i++)
		{
			int key = (int) (i);
			int val = (int) (Math.random()*1000);
			separateHash.put(key, val);
			linearProbingHash.put(key, val);
		}
		
	}

	@Test
	public void test() {
		System.out.println("N�mero de tuplas en Separate: "+ separateHash.size());
		System.out.println("N�mero de tuplas en linear: "+ linearProbingHash.size());
		System.out.println("Tama�o inicial en Separate: "+ 7);
		System.out.println("Tama�o inicial en linear: "+ 7);
		System.out.println("Tama�o inicial en Separate: "+ separateHash.sizeArray());
		System.out.println("Tama�o inicial en linear: "+ linearProbingHash.sizeArray());
		System.out.println("Factor de carga final en Separate: "+ (double)separateHash.size() / separateHash.sizeArray());
		System.out.println("Factor de carga final en linear: "+ (double)linearProbingHash.size()/ linearProbingHash.sizeArray());
		System.out.println("N�mero de rehashes que tuvo la tabla en Separate: "+ separateHash.getrehashes());
		System.out.println("N�mero de rehashes que tuvo la tabla en linear: "+ linearProbingHash.getrehashes());
		long promedioSeparate = 0;
		long promedioLinear = 0;
		for (int i=0;i<10000;i++)
		{
			int key = (int) (Math.random()*100000);
			long StartTime = System.nanoTime();
			separateHash.get(key);
			promedioSeparate += System.nanoTime() - StartTime;
			StartTime = System.nanoTime();
			linearProbingHash.get(key);
			promedioLinear += System.nanoTime() - StartTime;
		}
		System.out.println("Tiempo promedio de consultas get en Separate: "+ promedioSeparate/10000);
		System.out.println("Tiempo promedio de consultas get en linear: "+ promedioLinear/10000);
		
		
		
	}


}