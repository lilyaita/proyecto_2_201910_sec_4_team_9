

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.estructuras.Queue;


public class TestQueue  
{
	private Queue <String> queue;
	@Before
	public void setUpEscenario1()
	{
		queue = new Queue <String>();
	}
	public void setUpEscenario2() 
	{
		queue = new Queue <String> ("a");
	}
	public void setUpEscenario3() 
	{
		queue = new Queue <String> ("a");
		queue.enqueue("b");
		queue.enqueue("c");
	}
	@Test
	public void constructor() 
	{
		assertEquals( 0,queue.size());
		assertTrue (queue.isEmpty());
		setUpEscenario2();
		assertEquals( 1, queue.size());
		assertTrue (!queue.isEmpty());
		
		
	}
	
	@Test
	public void enAndDequeue() 
	{
		try
		{
			queue.enqueue("a");
			assertEquals(1,queue.size());
				
			queue.enqueue("e");
			assertEquals( 2,queue.size());
			
			queue.enqueue("b");
			assertEquals( 3,queue.size());
			
			assertEquals("a",queue.dequeue());
			assertEquals( 2,queue.size());
			
			assertEquals( "e", queue.dequeue());
			assertEquals( 1, queue.size());
			
			assertEquals("b",queue.dequeue());
			assertEquals( 0,queue.size());
			
		}catch( Exception e)
		{
			fail("Deberia poder agregar");
		}
		try 
		{
			queue.dequeue();
			fail("No se puede eliminar");
		}catch( Exception e)
		{
			//Respuesta correcta
		}
		
	}
	
	@Test
	public void iterador() 
	{
		int i=0;
		setUpEscenario3();
		String letras []={"a","b","c"};
		Iterator<String> it = (Iterator<String> )queue.iterator();
		while (it.hasNext())
		{
			assertEquals(letras[i],it.next());
			i++;
			
		}
		assertEquals(i, queue.size());
	}


}
